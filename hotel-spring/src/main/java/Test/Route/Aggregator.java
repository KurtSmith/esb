package Test.Route;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.EmptyStackException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import Test.Route.GetHotelResultsWithCacheV2.Results;
import Test.Route.GetHotelResultsWithCacheV2.Results.ResultsData;

import com.thoughtworks.xstream.XStream;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.json.XML;

public class Aggregator  implements AggregationStrategy {
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        // the first time we only have the new exchange
    	
        if (oldExchange == null) {
        	//System.out.println(newExchange.getIn().getClass());
        	System.out.println("bread");
        	//System.out.println( newExchange.getIn().getBody(String.class));
        	return newExchange;
        }
        else {
        	System.out.println("water");
        	 if( newExchange == null)
        		 return oldExchange;
        	 /*Document doc = convertStringToDocument(newExchange.getIn().getBody(String.class));
        	
        	 Element docEle = doc.getDocumentElement();
        	 NodeList nl = docEle.getChildNodes();
        	
        	 for (int i = 0; i < nl.getLength(); i++) {
        		 System.out.println(nl.item(i).getNodeType());
        	 }
        	// pull out some entries
        	
        	//for( object node = doc. )
        	
        	try {
        		DOMSource domSource = new DOMSource(doc);
                StringWriter writer = new StringWriter();
                StreamResult result = new StreamResult(writer);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.transform(domSource, result);
                writer.flush();
         
				JSONObject obj =  XML.toJSONObject(writer.toString());
				System.out.println("Hair on it");
				
			}
        	catch(Exception e)
			{
				e.printStackTrace();
			}*/
        	String newEx = newExchange.getIn().getBody(String.class);
        	String oldEx = oldExchange.getIn().getBody(String.class);
        	
        	JAXBContext jc = null;
			try {
				jc = JAXBContext.newInstance(GetHotelResultsWithCacheV2.class);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            Unmarshaller unmarshaller = null;
			try {
				unmarshaller = jc.createUnmarshaller();
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            try {
				GetHotelResultsWithCacheV2 root = (GetHotelResultsWithCacheV2) unmarshaller.unmarshal(new StringReader(newEx));
				Results results = root.getResults();
				
				for ( int i = 0; i < results.hotelData.hotel.size(); i++)
				{
					System.out.println(results.hotelData.hotel.get(i).name);
				}
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            //Marshaller marshaller = jc.createMarshaller();
            //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //marshaller.marshal(root, System.out);
        	//newEx = newEx.substring(0, (int)(newEx.length() * 0.3) );
        	//oldEx = oldEx.substring(0, (int)(oldEx.length() * 0.3) );
        	newExchange.getOut().setBody("<root>" + newEx + oldEx + "</root>");
            return newExchange;
        }
    }
    
    private static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
        DocumentBuilder builder;  
        try 
        {  
            builder = factory.newDocumentBuilder();  
            Document doc = builder.parse( new InputSource( new java.io.StringReader((xmlStr)) ) ); 
            return doc;
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        return null;
    }
}
